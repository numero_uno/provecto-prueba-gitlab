import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'primer-proyecto-ng'
  btnDisabled = true
  img = 'https://upload.wikimedia.org/wikipedia/commons/b/b7/Metallica_logo.png'

  person = {
    age: 20,
    name: "Israel",
    img: 'https://upload.wikimedia.org/wikipedia/commons/b/b7/Metallica_logo.png'
  }

  toggleButton(){
    this.btnDisabled = !this.btnDisabled
  }
  addAge(){
    this.person.age = this.person.age + 1
  }
  onScroll(event: Event){
    const element = event.target as HTMLElement
    console.log(element.scrollTop)
  }
  changeName(event: Event){
    const element = event.target as HTMLInputElement
    this.person.name = element.value;
  }
}
