Summary

(Da el resumen del issue)

Steps to reproduce

(Indica los pasos para reproducir el bug)

What is the current behavior?

(¿Cuál es el comportamiento actual?)

What is the expected behavior?

(¿Cuál es el comportamiento esperado?)
